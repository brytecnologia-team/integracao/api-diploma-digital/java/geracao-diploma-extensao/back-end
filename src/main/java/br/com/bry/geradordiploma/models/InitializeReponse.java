package br.com.bry.geradordiploma.models;

import br.com.bry.geradordiploma.models.InitializedAndSignedDocuments;

import java.util.ArrayList;
import java.util.List;


public class InitializeReponse {
	
	private int nonce;
	private List<InitializedAndSignedDocuments> initializedDocuments;
	private List<InitializedAndSignedDocuments> signedAttributes;
	
	public int getNonce() {
		return nonce;
	}
	
	public InitializeReponse() {
		this.initializedDocuments = new ArrayList<>();
		this.signedAttributes = new ArrayList<>();
	}

	public InitializeReponse(int nonce, List<InitializedAndSignedDocuments> initializedDocuments,
			List<InitializedAndSignedDocuments> signedAttributes) {
		this.nonce = nonce;
		this.initializedDocuments = initializedDocuments;
		this.signedAttributes = signedAttributes;
	}

	public void setNonce(int nonce) {
		this.nonce = nonce;
	}

	public List<InitializedAndSignedDocuments> getInitializedDocuments() {
		return initializedDocuments;
	}

	public void setInitializedDocuments(List<InitializedAndSignedDocuments> initializedDocuments) {
		this.initializedDocuments = initializedDocuments;
	}

	public List<InitializedAndSignedDocuments> getSignedAttributes() {
		return signedAttributes;
	}

	public void setSignedAttributes(List<InitializedAndSignedDocuments> signedAttributes) {
		this.signedAttributes = signedAttributes;
	}

	
}
