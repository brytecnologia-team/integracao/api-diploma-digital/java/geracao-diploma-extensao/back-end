package br.com.bry.geradordiploma.models;

public class InitializedAndSignedDocuments {
	
	String content;
	int nonce;
	
	public InitializedAndSignedDocuments() {

	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getNonce() {
		return nonce;
	}
	public void setNonce(int nonce) {
		this.nonce = nonce;
	}
}
