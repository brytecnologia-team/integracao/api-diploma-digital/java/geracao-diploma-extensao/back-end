package br.com.bry.geradordiploma.models;

public class InitializeRequest {
	
	private String certificate;
	private String tipoAssinante;
	
	public InitializeRequest(String certificate, String tipoAssinante) {
		this.certificate = certificate;
		this.tipoAssinante = tipoAssinante;
	}

	public String getCertificate() {
		return certificate;
	}
	
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	
	public String getTipoAssinante() {
		return tipoAssinante;
	}
	
	public void setTipoAssinante(String tipoAssinante) {
		this.tipoAssinante = tipoAssinante;
	}
	
}
