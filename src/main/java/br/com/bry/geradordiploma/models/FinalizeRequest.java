package br.com.bry.geradordiploma.models;

public class FinalizeRequest {
	
	private String certificate;
	private String tipoAssinante;
	private String initializedDocuments;
	private String cifrado;
	
	public String getCertificate() {
		return certificate;
	}
	
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	
	public String getTipoAssinante() {
		return tipoAssinante;
	}
	public void setTipoAssinante(String tipoAssinante) {
		this.tipoAssinante = tipoAssinante;
	}
	
	public String getInitializedDocuments() {
		return initializedDocuments;
	}
	
	public void setInitializedDocuments(String initializedDocuments) {
		this.initializedDocuments = initializedDocuments;
	}
	
	public String getCifrado() {
		return cifrado;
	}
	
	public void setCifrado(String cifrado) {
		this.cifrado = cifrado;
	}
	
	
}


