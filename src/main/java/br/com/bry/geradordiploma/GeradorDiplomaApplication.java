package br.com.bry.geradordiploma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeradorDiplomaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeradorDiplomaApplication.class, args);
	}

}
