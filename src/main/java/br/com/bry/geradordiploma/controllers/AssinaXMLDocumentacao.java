package br.com.bry.geradordiploma.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import br.com.bry.geradordiploma.models.FinalizeRequest;
import br.com.bry.geradordiploma.models.InitializeReponse;

@Controller
@RestController
@RequestMapping("/")
public class AssinaXMLDocumentacao {
	
	@Value("${token}")
	private String token;
	@Value("${urlInicializacao}")
	private String urlInicializacao;
	@Value("${urlFinalizacao}")
	private String urlFinalizacao;
	
	// PASSO 1 DA ASSINATURA: INICIALIZAÇÃO - RECEBE OS DADOS DA REQUISIÇÃO QUE VEM DO FRONT-END
	@PostMapping("/XMLDocumentacaoAcademica/inicializa")
	public InitializeReponse initialize(@RequestParam(value = "certificate", required = true) String certificate,
			@RequestParam(value = "tipoAssinante", required = true) String tipoAssinante,
			@RequestPart(value = "documento", required = true ) MultipartFile documento) throws IOException {
				
		InputStream inputStream = documento.getInputStream();
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_documentacaoAcademica_inicializar.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_documentacaoAcademica_inicializar.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		RestTemplate restTemplate = new RestTemplate();
		
		// CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add("Authorization", token);
		
		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
		
		form.add("nonce", 1);
		form.add("signatureFormat", "ENVELOPED");
		form.add("hashAlgorithm", "SHA256");
		form.add("certificate", certificate);
		form.add("originalDocuments[0][content]", new FileSystemResource(".temp/temp_documentacaoAcademica_inicializar.xml"));
		form.add("originalDocuments[0][nonce]", 1);
		form.add("returnType", "BASE64");
				
		// FAZ UM TIPO DE ASSINATURA DIFERENTE PARA CADA TIPO DE ASSINANTE
		switch (tipoAssinante) {
		
		case "Representantes":
		
			System.out.println("Tipo de Assinatura: IESRepresentantes");
			form.add("profile", "ADRC");
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
			// PODE SER NECESSÁRIO ALTERAR ESTE PARÂMETRO PARA DadosDiplomaNSF DE ACORDO COM O ARQUIVO SENDO UTILIZADO.
			form.add("originalDocuments[0][specificNode][name]", "DadosDiploma");
			
			break;
			
		case "IESEmissoraDadosDiploma":
			
			System.out.println("Tipo de Assinatura: IESEmissoraDadosDiploma");
			form.add("profile", "ADRC");
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
			// PODE SER NECESSÁRIO ALTERAR ESTE PARÂMETRO PARA DadosDiplomaNSF DE ACORDO COM O ARQUIVO SENDO UTILIZADO.
			form.add("originalDocuments[0][specificNode][name]", "DadosDiploma");
			form.add("includeXPathEnveloped", "false");
			
			break;
			
		case "IESEmissoraRegistro":
			
			System.out.println("Tipo de Assinatura: IESEmissoraRegistro");
			form.add("profile", "ADRA");
			form.add("includeXPathEnveloped", "false");
			
			break;
		}
		
		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);
		
		// FAZ A REQUISIÇÃO DE INICIALIZAÇÃO PARA A API DE ASSINATURA DA BRy
	    // RESPONDE PARA O FRONT O RETORNO DA API BRy PARA QUE SEJA
	    // REALIZADA A CIFRAGEM COM A EXTENSÃO DA BRY
		ResponseEntity<InitializeReponse> response = restTemplate.exchange(
				urlInicializacao, 
				HttpMethod.POST, entity, InitializeReponse.class);
		
		tempFile.delete();
		
		return response.getBody();
	}
	
	// PASSO 3 DA ASSINATURA: FINALIZAÇÃO - RECEBE OS DADOS DA REQUISIÇÃO QUE VEM DO FRONT
	@PostMapping("/XMLDocumentacaoAcademica/finaliza")
	public String finalize(@RequestParam(value = "certificate", required = true) String certificate,
			@RequestParam(value = "tipoAssinante", required = true) String tipoAssinante,
			@RequestParam(value = "initializedDocuments", required = true) String initializedDocuments,
			@RequestParam(value = "cifrado", required = true) String cifrado,
			@RequestPart(value = "documento", required = true) MultipartFile documento) throws IOException {
		
		//	PARA SALVAR O DOCUMENTO NO BACKEND COLOCAR O VALOR "BASE64"
		//	PARA DOWNLOAD NO FRONTEND COLOCAR O VALOR "LINK"	
		String returnType = "BASE64";
		
		InputStream inputStream = documento.getInputStream();
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_documentacaoAcademica_finalizar.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_documentacaoAcademica_finalizar.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		RestTemplate restTemplate = new RestTemplate();
		
		// CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add("Authorization", token);
		
		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
		
		// ADICIONA DADOS NO FORMDATA REFERENTE AO TIPO DE ASSINATURA
		form.add("nonce", 1);
		form.add("signatureFormat", "ENVELOPED");
		form.add("hashAlgorithm", "SHA256");
		form.add("certificate", certificate);
		form.add("profile", tipoAssinante.equals("IESEmissoraRegistro") ? "ADRA" : "ADRC");
		form.add("returnType", returnType);
		form.add("finalizations[0][content]", new FileSystemResource(".temp/temp_documentacaoAcademica_finalizar.xml"));
		form.add("originalDocuments[0][nonce]", 1);
		form.add("finalizations[0][signatureValue]", cifrado);
		form.add("finalizations[0][initializedDocument]", initializedDocuments);
		
		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);
		
		// FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA A API DE ASSINATURA DA BRy
	    // RESPONDE PARA O FRONT O RETORNO DA API BRy

	   
	     // CONSIDERANDO QUE O RETURNTYPE ESTÁ CONFIGURADO COMO 'BASE64' NA REQUISIÇÃO,
	     // ESTAMOS SALVANDO O ARQUIVO ASSINADO NA PASTA "XMLsAssinados".

		if(returnType == "LINK") {
			
			ResponseEntity<String> response = restTemplate.exchange(
					urlFinalizacao, 
					HttpMethod.POST, entity, String.class);
			
			tempFile.delete();
			
			return response.getBody();
		} else {
			
			ResponseEntity<String[]> response = restTemplate.exchange(
					urlFinalizacao, 
					HttpMethod.POST, entity, String[].class);
			byte[] assinatura = Base64.getDecoder().decode(response.getBody()[0]);
			File diploma = new File("XMLsAssinados/" + "exemplo-diploma-documentacao-assinado-" + tipoAssinante + ".xml");
			FileOutputStream diplomaAssinado = new FileOutputStream(diploma);
			diplomaAssinado.write(assinatura);
			diplomaAssinado.close();
			
			tempFile.delete();
			
			return "{message: 'Arquivo assinado e armazenado localmente'}";
		}
				
	}

}
