package br.com.bry.geradordiploma.controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;

import br.com.bry.geradordiploma.models.FinalizeRequest;
import br.com.bry.geradordiploma.models.InitializeReponse;
import br.com.bry.geradordiploma.models.InitializeRequest;

@Controller
@RestController
@RequestMapping("/")
public class AssinaXMLDiplomado {
	
	@Value("${token}")
	private String token;
	@Value("${urlInicializacao}")
	private String urlInicializacao;
	@Value("${urlFinalizacao}")
	private String urlFinalizacao;
	
	// PASSO 1 DA ASSINATURA: INICIALIZAÇÃO - RECEBE OS DADOS DA REQUISIÇÃO QUE VEM DO FRONT
	@PostMapping("/XMLDiplomado/inicializa")
	public InitializeReponse initialize(@RequestParam(value = "certificate", required = true) String certificate,
			@RequestParam(value = "tipoAssinante", required = true) String tipoAssinante,
			@RequestPart(value = "documento", required = true) MultipartFile documento)
			throws IOException, ParserConfigurationException, SAXException,
			TransformerFactoryConfigurationError, TransformerException {
		
		InputStream inputStream = documento.getInputStream();
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_XMLDiplomado_inicializa.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_XMLDiplomado_inicializa.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		RestTemplate restTemplate = new RestTemplate();
		
		// CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add("Authorization", token);
		
		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
		
		form.add("nonce", 1);
		form.add("signatureFormat", "ENVELOPED");
		form.add("hashAlgorithm", "SHA256");
		form.add("certificate", certificate);
		form.add("originalDocuments[0][content]", new FileSystemResource(".temp/temp_XMLDiplomado_inicializa.xml"));
		form.add("originalDocuments[0][nonce]", 1);
		form.add("returnType", "BASE64");
		
		// FAZ UM TIPO DE ASSINATURA DIFERENTE PARA CADA TIPO DE ASSINANTE
		switch (tipoAssinante) {
			
		case "Representantes":
			
			System.out.println("Tipo de Assinatura: Representantes");
			form.add("profile", "ADRC");
			form.add("originalDocuments[0][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
			// PODE SER NECESSÁRIO ALTERAR ESTE PARÂMETRO PARA DadosRegistroNSF DE ACORDO COM O ARQUIVO SENDO UTILIZADO.
			form.add("originalDocuments[0][specificNode][name]", "DadosRegistro");
			
			break;
			
		case "IESRegistradora":
			
			System.out.println("Tipo de Assinatura: IESRegistradora");
			form.add("profile", "ADRA");
			form.add("includeXPathEnveloped", "false");
			
			break;
		}
		
		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);
		
		// FAZ A REQUISIÇÃO DE INICIALIZAÇÃO PARA A API DE ASSINATURA DA BRy
	    // RESPONDE PARA O FRONT O RETORNO DA API BRy PARA QUE SEJA
	    // REALIZADA A CIFRAGEM COM A EXTENSÃO DA BRY
		ResponseEntity<InitializeReponse> response = restTemplate.exchange(
				urlInicializacao, 
				HttpMethod.POST, entity, InitializeReponse.class);
		
		tempFile.delete();
		
		return response.getBody();
	}
	
	// PASSO 3 DA ASSINATURA: FINALIZAÇÃO - RECEBE OS DADOS DA REQUISIÇÃO QUE VEM DO FRONT
	@PostMapping("/XMLDiplomado/finaliza")
	public String finalize(@RequestParam(value = "certificate", required = true) String certificate,
			@RequestParam(value = "tipoAssinante", required = true) String tipoAssinante,
			@RequestParam(value = "initializedDocuments", required = true) String initializedDocuments,
			@RequestParam(value = "cifrado", required = true) String cifrado,
			@RequestPart(value = "documento", required = true) MultipartFile documento) throws IOException {
		
		//	PARA SALVAR O DOCUMENTO NO BACKEND COLOCAR O VALOR "BASE64"
		//	PARA DOWNLOAD NO FRONTEND COLOCAR O VALOR "LINK"	
		String returnType = "BASE64";
		
		InputStream inputStream = documento.getInputStream();
		byte[] buffer = new byte[2048];
		File tempFile = new File(".temp/temp_XMLDiplomado_finaliza.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_XMLDiplomado_finaliza.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		RestTemplate restTemplate = new RestTemplate();
		
		// CRIA O HEADER DA REQUISIÇÃO COM O TOKEN CONFIGURADO ANTERIORMENTE
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add("Authorization", token);
		
		MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
		
		// ADICIONA DADOS NO FORMDATA REFERENTE AO TIPO DE ASSINATURA
		form.add("nonce", 1);
		form.add("signatureFormat", "ENVELOPED");
		form.add("hashAlgorithm", "SHA256");
		form.add("certificate", certificate);
		form.add("includeXPathEnveloped", tipoAssinante.equals("IESRegistradora") ? "false" : "true");
		form.add("profile", tipoAssinante.equals("IESRegistradora") ? "ADRA" : "ADRC");
		form.add("returnType", returnType);
		form.add("finalizations[0][content]", new FileSystemResource(".temp/temp_XMLDiplomado_finaliza.xml"));
		form.add("originalDocuments[0][nonce]", 1);
		form.add("finalizations[0][signatureValue]", cifrado);
		form.add("finalizations[0][initializedDocument]", initializedDocuments);
		
		HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);
		
		// FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA A API DE ASSINATURA DA BRy
	    // RESPONDE PARA O FRONT O RETORNO DA API BRy

	   
		//	PARA SALVAR O DOCUMENTO NO BACKEND COLOCAR O VALOR "BASE64"
		//	PARA DOWNLOAD NO FRONTEND COLOCAR O VALOR "LINK"	

		if(returnType == "LINK") {
			
			ResponseEntity<String> response = restTemplate.exchange(
					urlFinalizacao, 
					HttpMethod.POST, entity, String.class);
			
			tempFile.delete();
			
			return response.getBody();
		} else {
			
			ResponseEntity<String[]> response = restTemplate.exchange(
					urlFinalizacao, 
					HttpMethod.POST, entity, String[].class);
			byte[] assinatura = Base64.getDecoder().decode(response.getBody()[0]);
			File diploma = new File("XMLsAssinados/" + "exemplo-xml-diplomado-assinado-" + tipoAssinante + ".xml");
			FileOutputStream diplomaAssinado = new FileOutputStream(diploma);
			diplomaAssinado.write(assinatura);
			diplomaAssinado.close();
			
			tempFile.delete();
			
			return "{message: 'Arquivo assinado e armazenado localmente'}";
		}
				
	}
	
	@PostMapping(value = "/XMLDiplomado/copia-nodo", produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<?> copiaNodo(
			@RequestPart(value = "documentacaoAcademica", required = true) MultipartFile documentacaoAcademica,
			@RequestPart(value = "XMLDiplomado", required = true) MultipartFile xMLDiplomado)
					throws IOException, ParserConfigurationException, SAXException,
					TransformerFactoryConfigurationError, TransformerException {
		
		
		InputStream inputStream = documentacaoAcademica.getInputStream();
		File tempFile = new File(".temp/temp_documentacaoAcademica.xml");
		if(tempFile.exists()) {
			tempFile.delete();
			if(!tempFile.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_documentacaoAcademica.xml");
		}
		FileOutputStream outputStream = new FileOutputStream(tempFile);
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		
		
		InputStream inputStream2 = xMLDiplomado.getInputStream();
		File tempFile2 = new File(".temp/temp_XMLDiplomado.xml");
		if(tempFile2.exists()) {
			tempFile2.delete();
			if(!tempFile2.createNewFile())
				throw new RuntimeException("Falha ao criar arquivo temporario no diretorio .temp/temp_XMLDiplomado.xml");
		}
		FileOutputStream outputStream2 = new FileOutputStream(tempFile2);
		buffer = new byte[2048];
		while ((bytesRead = inputStream2.read(buffer)) != -1) {
			outputStream2.write(buffer, 0, bytesRead);
		}
		outputStream2.close();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		
		DocumentBuilder db = dbf.newDocumentBuilder();
		
		Document doc = db.parse(new FileInputStream(new File(".temp/temp_documentacaoAcademica.xml")));
		doc.getDocumentElement().normalize();
		
		Node registroReqNode = doc.getElementsByTagName("RegistroReq").item(0);
		Element registroReq = (Element) registroReqNode;
		
		NodeList dadosDiplomaList = registroReq.getElementsByTagName("DadosDiploma");
		if (dadosDiplomaList.getLength() == 0) {
			dadosDiplomaList = registroReq.getElementsByTagName("DadosDiplomaNSF");
		}
		Node dadosDiplomaNode = dadosDiplomaList.item(0);
						
		DocumentBuilder dbDiplomado = dbf.newDocumentBuilder();
		Document docDiplomado = dbDiplomado.parse(new FileInputStream(new File(".temp/temp_XMLDiplomado.xml")));
		
		docDiplomado.getDocumentElement().normalize();
		
		Node infDiplomaNode = docDiplomado.getElementsByTagName("infDiploma").item(0);
		Element infDiploma = (Element) infDiplomaNode;
		
		infDiploma.insertBefore(docDiplomado.adoptNode(dadosDiplomaNode.cloneNode(true)), infDiploma.getFirstChild());
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		Result output = new StreamResult(new File("XMLsAssinados/XMLDiplomadoCompleto.xml"));
		Source input = new DOMSource(docDiplomado);
		System.out.println(docDiplomado.toString());	
		transformer.transform(input, output);
		
		File file = new File("XMLsAssinados/XMLDiplomadoCompleto.xml");
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		buffer = new byte[2048];
		while ((bytesRead = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, bytesRead);
		}
		fis.close();
		
		tempFile.delete();
		tempFile2.delete();
		
		byte[] xMLDiplomadoCompleto = baos.toByteArray();
		
		return ResponseEntity.ok(new String(xMLDiplomadoCompleto, "UTF-8"));
	}

}
